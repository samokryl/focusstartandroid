package ru.ftc.focus.rx;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import ru.ftc.focus.rx.helpers.CatShelterHelper;
import ru.ftc.focus.rx.helpers.CatWasher;
import ru.ftc.focus.rx.helpers.PotentialNewOwner;
import ru.ftc.focus.rx.model.Cat;

/**
 * User: asmoljak
 * Date: 10.05.2018
 * Time: 14:00
 */
public class Lesson2Test {

	private Lesson2 lesson;

	@Before
	public void setUp() {
		lesson = new Lesson2();
	}

	@Test
	public void testCatNameToCats() {
		final TestObserver<List<Cat>> test = lesson.catNameToCat(CatShelterHelper.getCatNames()).toList().test();
		test.assertValue( cats -> cats.size() == 5);
		test.assertValue(cats -> cats.contains(new Cat("MeatBall")));
	}

	@Test
	public void testCatNameAndOwnerNameToCat() {
		final TestObserver<Cat> test = lesson.catNameAndOwnerNameToCat(CatShelterHelper.getCatNames(), CatShelterHelper.getOwnerNames()).test();
		test.assertValueCount(5);
		test.assertValueAt(0, new Cat("Teon", "Ivan Borisovich"));
		test.assertValueAt(1, new Cat("Petya", "Olga"));
//		test.assertValues(new Cat("Teon", "Sergey"), new Cat("Petya", "Olga"));
	}

	@Test
	public void testGetTwoCatNames() {
		final TestObserver<String> test = lesson.getTwoCatNames(CatShelterHelper.getCatNames()).test();
		test.assertValueAt(0, "Teon");
		test.assertValueAt(1, "Petya");
		test.assertValueCount(2);
	}

	@Test
	public void testGetCatsOnly() {
		final TestObserver<Cat> test = lesson.getCatsOnly(CatShelterHelper.getAnimals()).test();
		test.assertValues(new Cat("SomeCat"), new Cat("AnotherCat"));
		test.assertValueCount(2);
	}

	@Test
	public void testShowCats() {
		final TestObserver<Cat> test = lesson.showCats(lesson.catNameToCat(CatShelterHelper.getCatNames()), new PotentialNewOwner()).test();
		test.assertValue(new Cat("MeatBall", PotentialNewOwner.OWNER_NAME));
	}

	@Test
	public void testGetCatsAndKittens() {
		final TestObserver<Cat> test = lesson.getCatsAndKittens(CatShelterHelper.getCatNames(), CatShelterHelper.getKittenNames()).test();
		test.assertValueAt(0, new Cat("Teon"));
		test.assertValueAt(1, new Cat("Petya"));
		test.assertValueAt(6, new Cat("Thanatos"));
		test.assertValueCount(7);
	}

	@Test
	public void testCatWasher() throws InterruptedException {
		final Observable<Cat> cats = lesson.getCatsAndKittens(CatShelterHelper.getCatNames(), CatShelterHelper.getKittenNames());
		final TestObserver<Void> test = lesson.washCat(cats, "Thanatos", new CatWasher()).test();
		test.await();
		test.assertComplete();
	}


}