package ru.ftc.focus.rx;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static ru.ftc.focus.rx.ObservableExample.*;

/**
 * Created: krilov
 * Date: 07.05.2018
 * Time: 14:51
 */
public class ObservableExampleTest {

    private ObservableExample example;

    @Before
    public void setUp() throws Exception {
        example = new ObservableExample();
    }

    @Test
    public void observableEx1() {
        example.observableEx1()
                .test()
                .assertValue(VALUE_1)
                .assertNotComplete()
                .assertNoErrors();
    }

    @Test
    public void observableEx2() {
        example.observableEx2()
                .test()
                .assertValues(new String[]{VALUE_4, VALUE_2, VALUE_3, VALUE_1})
                .assertNotComplete()
                .assertNoErrors();
    }

    @Test
    public void observableEx3() {
        example.observableEx3()
                .test()
                .assertValue(VALUE_4)
                .assertComplete();
    }

    @Test
    public void observableEx4() {
        example.observableEx4()
                .test()
                .assertValues(new String[]{VALUE_1, VALUE_4, VALUE_2, VALUE_3})
                .assertComplete();
    }

    @Test
    public void observableEx5() {
        example.observableEx5()
                .test()
                .assertError(IllegalArgumentException.class)
                .assertErrorMessage(VALUE_2);
    }

    @Test
    public void observableEx6() {
        example.observableEx6()
                .test()
                .assertValues(new String[]{VALUE_1, VALUE_2})
                .assertComplete();
    }

    @Test
    public void observableEx7() {
        example.observableEx7()
                .test()
                .assertValues(new String[]{VALUE_1, VALUE_2})
                .assertNotComplete();
    }

    @Test
    public void observableEx8() {
        example.observableEx8()
                .test()
                .assertValues(new String[]{VALUE_1, VALUE_2, VALUE_1})
                .assertComplete();
    }

    @Test
    public void observableEx9() {
        example.observableEx9()
                .test()
                .assertValues(new String[]{VALUE_4, VALUE_2})
                .assertNotComplete();
    }

    @Test
    public void observableEx10() {
        example.observableEx10()
                .test()
                .assertValues(new String[]{VALUE_1, VALUE_2, VALUE_1})
                .assertNotComplete();
    }

    @Test
    public void observableEx11() {

        final int length = 40000;
        final String[] expected = new String[length];

        for (int i = 0; i < length; i += 4) {
            expected[i] = VALUE_1;
            expected[i + 1] = VALUE_2;
            expected[i + 2] = VALUE_3;
            expected[i + 3] = VALUE_4;
        }

        example.observableEx11()
                .test()
                .assertValueCount(length)
                .assertValues(expected)
                .assertComplete();
    }

    @Test
    public void observableEx12() {
        example.observableEx12()
                .test()
                .assertValue(VALUE_1)
                .assertComplete();
    }

    @Test
    public void observableEx13() {
        example.observableEx13()
                .test()
                .assertValues(new String[]{VALUE_4, VALUE_2, VALUE_3, VALUE_1})
                .assertComplete();
    }

    @Test
    public void observableEx14() {
        example.observableEx14()
                .test()
                .assertValue(VALUE_4)
                .assertComplete();
    }

    @Test
    public void observableEx15() {
        example.observableEx15()
                .test()
                .assertValues(new String[]{VALUE_1, VALUE_4, VALUE_2, VALUE_3})
                .assertComplete();
    }

    @Test
    public void observableEx16() {
        example.observableEx16()
                .test()
                .assertError(IllegalArgumentException.class)
                .assertErrorMessage(VALUE_2);
    }

    @Test
    public void observableEx17() {
        example.observableEx17()
                .test()
                .assertValues(new String[]{VALUE_1, VALUE_4})
                .assertComplete();
    }

    @Test
    public void observableEx18() {
        example.observableEx18()
                .test()
                .assertValues(new String[]{VALUE_1, VALUE_2})
                .assertComplete();
    }

    @Test
    public void observableEx19() {
        example.observableEx19()
                .test()
                .assertValues(new String[]{VALUE_1, VALUE_2, VALUE_4})
                .assertComplete();
    }

    @Test
    public void observableEx20() {
        example.observableEx20()
                .test()
                .assertValues(new String[]{VALUE_4, VALUE_3, VALUE_1})
                .assertComplete();
    }

    @Test
    public void observableEx21() {
        example.observableEx21()
                .test()
                .assertValues(new String[]{VALUE_3, VALUE_2, VALUE_1})
                .assertComplete();
    }

    @Test
    @Ignore
    public void observableEx22() {
        final int length = 10000;
        final Integer[] expected = new Integer[length];

        for (int i = 0; i < length; i++) {
            expected[i] = i;
        }

//        example.observableEx22()
//                .test()
//                .assertValueCount(length)
//                .assertValues(expected)
//                .assertComplete();
    }

    @Test
    public void observableEx23() {
        example.observableEx23()
                .test()
                .assertError(RuntimeException.class)
                .assertErrorMessage(VALUE_4);
    }

    @Test
    public void observableEx24() {
        example.observableEx24()
                .test()
                .assertError(RuntimeException.class)
                .assertErrorMessage(VALUE_1);
    }

}