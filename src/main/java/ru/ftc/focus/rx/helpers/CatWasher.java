package ru.ftc.focus.rx.helpers;

import ru.ftc.focus.rx.model.Cat;

/**
 * User: asmoljak
 * Date: 10.05.2018
 * Time: 13:54
 */
public final class CatWasher {

	public boolean washCat(final Cat cat) {
		System.out.println("Washing cat: " + cat);
		try {
			Thread.sleep(2000);
		} catch (final InterruptedException ignored) {
		}
		System.out.println("Cat washed: " + cat);
		return true;
	}
}
