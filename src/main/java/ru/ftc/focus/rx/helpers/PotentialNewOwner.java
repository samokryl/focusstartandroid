package ru.ftc.focus.rx.helpers;

import io.reactivex.Observable;
import io.reactivex.Single;
import ru.ftc.focus.rx.model.Cat;

/**
 * User: asmoljak
 * Date: 10.05.2018
 * Time: 13:45
 */
public final class PotentialNewOwner {

	public static final String OWNER_NAME = "Stepan";

	public Single<Cat> showCats(final Observable<Cat> cats) {
		return cats.takeLast(1).map(cat -> new Cat(cat.getName(), OWNER_NAME)).singleOrError();
	}
}
