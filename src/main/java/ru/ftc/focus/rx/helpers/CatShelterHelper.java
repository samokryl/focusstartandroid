package ru.ftc.focus.rx.helpers;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;
import ru.ftc.focus.rx.model.Animal;

/**
 * User: asmoljak
 * Date: 10.05.2018
 * Time: 13:27
 */
public final class CatShelterHelper {

	private CatShelterHelper() {
		throw new UnsupportedOperationException("Illegal constructor call");
	}

	public static Observable<String> getCatNames() {
		final List<String> cats = Arrays.asList("Teon", "Petya", "Warrior", "Spooky", "MeatBall");
		return Observable.fromIterable(cats);
	}

	public static Observable<String> getKittenNames() {
		final List<String> kittens = Arrays.asList("Cheezy", "Thanatos");
		return Observable.fromIterable(kittens);
	}

	public static Observable<String> getOwnerNames() {
		final List<String> owners = Arrays.asList("Ivan Borisovich", "Olga", "Anon", "Grisha", "Inna");
		return Observable.fromIterable(owners);
	}

	public static Observable<Animal> getAnimals() {
		final List<Animal> animals = Arrays.asList(new Animal("SomeCat", Animal.Type.CAT),
												   new Animal("SomeDog", Animal.Type.DOG),
												   new Animal("SomeAlien", Animal.Type.ALIEN),
												   new Animal("AnotherCat", Animal.Type.CAT));
		return Observable.fromIterable(animals);
	}

}
