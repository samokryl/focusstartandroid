package ru.ftc.focus.rx;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import ru.ftc.focus.rx.helpers.CatWasher;
import ru.ftc.focus.rx.helpers.PotentialNewOwner;
import ru.ftc.focus.rx.model.Animal;
import ru.ftc.focus.rx.model.Cat;

/**
 * User: asmoljak
 * Date: 10.05.2018
 * Time: 11:51
 */
public class Lesson2 {

	//Вернуть список котиков но основании их имен
	public Observable<Cat> catNameToCat(final Observable<String> catNames) {
		return null;
	}

	//Вернуть список котиков всесте с хозяевами
	public Observable<Cat> catNameAndOwnerNameToCat(final Observable<String> catNames, final Observable<String> ownerNames) {
		return null;
	}

	//Вернуть имена 2х первых котиков
	public Observable<String> getTwoCatNames(final Observable<String> catNames) {
		return null;
	}

	//Отфильтровать только котов
	public Observable<Cat> getCatsOnly(final Observable<Animal> animals) {
		return null;
	}

	//Показать котов потенциальному покупателю, вернуть выбранного кота
	public Maybe<Cat> showCats(final Observable<Cat> cats, final PotentialNewOwner potentialNewOwner) {
		return null;
	}

	//Вернуть список принятых в приют котов и котят
	public Observable<Cat> getCatsAndKittens(final Observable<String> cats, final Observable<String> kittens) {
		return null;
	}

	//Вернуть Completable, который завершится, когда кот с соответствующим именем будет помыт
	public Completable washCat(final Observable<Cat> cats, final String catName, final CatWasher washer) {
		return null;
	}
}
