package ru.ftc.focus.rx.model;

import java.util.Objects;

/**
 * User: asmoljak
 * Date: 10.05.2018
 * Time: 11:52
 */
public final class Cat {

	private static final String SHELTER_NAME = "CAR SHELTER";

	private final String name;
	private final String ownerName;

	public Cat(final String name) {
		this(name, SHELTER_NAME);
	}

	public Cat(final String name, final String ownerName) {
		this.name = name;
		this.ownerName = ownerName;
	}

	public String getName() {
		return name;
	}

	public String getOwnerName() {
		return ownerName;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }
		final Cat cat = (Cat) o;
		return Objects.equals(name, cat.name) &&
			Objects.equals(ownerName, cat.ownerName);
	}

	@Override
	public int hashCode() {

		return Objects.hash(name, ownerName);
	}
}
