package ru.ftc.focus.rx.model;

/**
 * User: asmoljak
 * Date: 10.05.2018
 * Time: 13:22
 */
public class Animal {

	public enum Type {
		CAT,
		DOG,
		ALIEN
	}

	private final String name;
	private final Type type;

	public Animal(final String name, final Type type) {
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}
}
