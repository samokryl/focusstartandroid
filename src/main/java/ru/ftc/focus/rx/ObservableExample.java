package ru.ftc.focus.rx;

import io.reactivex.Observable;

/**
 * Created: krilov
 * Date: 07.05.2018
 * Time: 14:51
 */
class ObservableExample {

    static final String VALUE_1 = "1";
    static final String VALUE_2 = "2";
    static final String VALUE_3 = "3";
    static final String VALUE_4 = "4";

    /**
     * Передать в observable значение VALUE_1,
     * используя create()
     */
    Observable<String> observableEx1() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_4, VALUE_2, VALUE_3, VALUE_1,
     * используя create()
     */
    Observable<String> observableEx2() {
        return null;
    }

    /**
     * Передать в observable VALUE_4 и завершить поток,
     * используя create()
     */
    Observable<String> observableEx3() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_1, VALUE_4, VALUE_2, VALUE_3 и завершить поток,
     * используя create()
     */
    Observable<String> observableEx4() {
        return null;
    }

    /**
     * Бросить исключение IllegalArgumentException с текстом VALUE_2,
     * используя create()
     */
    Observable<String> observableEx5() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_1, VALUE_2 и завершить поток,
     * используя create()
     */
    Observable<String> observableEx6() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_1, VALUE_2,
     * используя create()
     */
    Observable<String> observableEx7() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_1, VALUE_2, VALUE_1 и завершить поток,
     * используя create()
     */
    Observable<String> observableEx8() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_4, VALUE_2,
     * используя create()
     */
    Observable<String> observableEx9() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_1, VALUE_2, VALUE_1,
     * используя create()
     */
    Observable<String> observableEx10() {
        return null;
    }

    /**
     * Передать в observable последовательность из 40000 элементов,
     * с повторяющимися значениями VALUE_1, VALUE_2, VALUE_3, VALUE_4 и завершить поток,
     * используя create()
     */
    Observable<String> observableEx11() {
        return null;
    }

    /**
     * Передать в observable значение VALUE_1,
     * используя just()
     */
    Observable<String> observableEx12() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_4, VALUE_2, VALUE_3, VALUE_1,
     * используя just()
     */
    Observable<String> observableEx13() {
        return null;
    }

    /**
     * Передать в observable VALUE_4,
     * используя just()
     */
    Observable<String> observableEx14() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_1, VALUE_4, VALUE_2, VALUE_3,
     * используя just()
     */
    Observable<String> observableEx15() {
        return null;
    }

    /**
     * Бросить исключение IllegalArgumentException с текстом VALUE_2,
     * используя error()
     */
    Observable<String> observableEx16() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_1, VALUE_4,
     * используя just()
     */
    Observable<String> observableEx17() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_1, VALUE_2,
     * используя just()
     */
    Observable<String> observableEx18() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_1, VALUE_2, VALUE_4,
     * используя just()
     */
    Observable<String> observableEx19() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_4, VALUE_3, VALUE_1,
     * используя just()
     */
    Observable<String> observableEx20() {
        return null;
    }

    /**
     * Передать в observable последовательно VALUE_3, VALUE_2, VALUE_1,
     * используя just()
     */
    Observable<String> observableEx21() {
        return null;
    }

    /**
     * Передать в observable последовательность из 10000 элементов,
     * с значениями от 0 до 9999, используя range()
     */
    Observable<String> observableEx22() {
        return null;
    }

    /**
     * Бросить исключение RuntimeException с текстом VALUE_4,
     * используя error()
     */
    Observable<String> observableEx23() {
        return null;
    }

    /**
     * Бросить исключение RuntimeException с текстом VALUE_1,
     * используя create()
     */
    Observable<String> observableEx24() {
        return null;
    }

}
